const amqp = require('amqplib/callback_api');

//Step1: Create Connection
amqp.connect('amqp://localhost', (connError, connection) => {
    if(connError) {
        throw connError;
    }

    // Step2: craete a channnel
    connection.createChannel((channelError, channel) => {
        if(channelError)
        {
            throw channelError;
        
        }
    // Step3: Assert Queue
    const QUEUE = 'RabbitMQ';
    channel.assertQueue(QUEUE);
    // Step4: Send Message to queue
    channel.sendToQueue(QUEUE, Buffer.from("Hello Hoooman"));
    console.log(`Message Send ${QUEUE}`);
    })
})